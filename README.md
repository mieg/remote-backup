[![pipeline status](https://framagit.org/mieg/remote-backup/badges/master/pipeline.svg)](https://framagit.org/mieg/remote-backup)

remote-backup (rbak)
====================

rbak is a cli tool that wake up a server and perform backup on it.

See help for more info.



Installation
------------

rbak needs python version >= 3.5. So ensure `pip` points to a correct
version of python. To do this run:
```shell
pip --version
```

It should return something like:
```
pip xx.y from /path/to/pip (python 3.5)
```

If `pip` doesn't run python >=3.5, try running `pip3` which is on
certain distribution the `pip` for python >=3.


### Dependencies

rbak uses external programs via the shell. Be sure they are installed
and accessible for the current user.

- rsync

rbak use cryptograhy python module. Install required dependencies of
cryptograhpy in order to install rbak.
```shell
sudo apt install build-essential libssl-dev libffi-dev python3-dev
```


### Install with pipx (recommended)

```shell
pipx install --spec git+https://framagit.org/mieg/remote-backup.git rbak
```


### Install with pipsi

```shell
pipsi install git+https://framagit.org/mieg/remote-backup.git#egg=rbak
```


### Install with pip

For a specific user:
```shell
pip install --user git+https://framagit.org/mieg/remote-backup.git#egg=rbak
```

For all users:
```shell
sudo pip install git+https://framagit.org/mieg/remote-backup.git#egg=rbak
```


### Enable bash completion

To enable bash completion add the following in your `.bashrc`:

```shell
eval "$(_RBAK_COMPLETE=source rbak)"
```

Or if you use zsh, add this to your `.zshrc`:
```shell
eval "$(_RBAK_COMPLETE=source_zsh rbak)"
```



Upgrade
-------


### Upgrade with pipx

```shell
pipx upgrade --spec git+https://framagit.org/mieg/remote-backup.git rbak
```


### Upgrade with pipsi

```shell
pipsi upgrade git+https://framagit.org/mieg/remote-backup.git#egg=rbak
```


### Upgrade with pip

For a specific user:
```shell
pip install --user --upgrade git+https://framagit.org/mieg/remote-backup.git#egg=rbak
```
