from setuptools import setup

import rbak


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='rbak',
    version=rbak.__version__,
    description=rbak.__description__,
    author=rbak.__author__,
    author_email=rbak.__author_email__,
    url=rbak.__url__,
    license=rbak.__license__,
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=['rbak'],
    install_requires=[
        'fabric',
        'click',
        'click-spinner',
        'wakeonlan',
    ],
    package_data={
        'rbak': [
            'defaults/rbak.conf',
        ]
    },
    entry_points={
        'console_scripts': [
            'rbak = rbak.cli:main',
        ]
    },
    provides=['rbak'],
    python_requires=">=3.5",
)
