# Copyright 2019 Rémy Taymans <remy.mieg@gmail.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""This file contains tools to manage the configuration file"""

from configparser import ConfigParser
from os import environ
from pathlib import Path


class SettingsManager(ConfigParser):
    """Handel configuration of the program"""

    def __init__(
        self,
        pgr_name,
        file_ext,
        defaults_file='',
        config_files=None,
        merge=True,
        **kw
    ):
        """
        Create a ConfigParser with default values and specific
        configuration.

        :param pgr_name: name of the program used to determine
                         configuration file name and configuration file
                         location.
        :type pgr_name: str

        :param file_ext: extention added to the configuration file name.
        :type file_ext: [str]

        :param defaults_file: path to a file containing a default
                              configuration.
        :type defaults_file: str

        :param config_files: list of path to the configuration files the
                             first is the most important, the last is
                             the less important.
        :type config_files: [str]

        :param merge: Merge all configuration file found in
                      :config_files: if given else in the default config
                      file locations.
        """
        converters = {
            'list': lambda lst: [
                elem.strip() for elem in filter(bool, lst.split(','))
            ],
        }
        super().__init__(converters=converters, **kw)
        self.pgr_name = pgr_name
        self.file_ext = file_ext
        self.defaults_file = defaults_file
        self.config_files = config_files
        self.merge = merge
        self.config_sources = None

    def load(self):
        """Loads defaults and configurations files"""
        # Loads defaults value if provided
        if self.defaults_file:
            defaults_file_resolved = str(
                Path(self.defaults_file).expanduser().resolve()
            )
            with open(defaults_file_resolved) as file:
                self.read_file(file)
        # Resolve config files
        if self.config_files:
            config_files_resolved = [
                str(Path(p).expanduser().resolve())
                for p in self.config_files if Path(p).expanduser().exists()
            ]
        else:
            config_files_resolved = [
                str(Path(p).expanduser().resolve())
                for p in self.default_config_files
                if Path(p).expanduser().exists()
            ]
        if self.merge:
            # Reverse order of the config files because ConfigParser will
            # retain the last value loaded.
            config_files_resolved.reverse()
            # Loads config files
            self.config_sources = self.read(config_files_resolved)
        else:
            for file in config_files_resolved:
                self.config_sources = self.read(file)
                if self.config_sources:
                    break

    @property
    def default_config_files(self):
        """Return a list of files to look for configuration."""
        xdg_config_home = environ.get(
            'XDG_CONFIG_HOME', str(Path.home() / Path('.config'))
        )
        xdg_config_dirs = environ.get(
            'XDG_CONFIG_DIRS', str(Path('/etc', 'xdg'))
        ).split(':')
        config_file_names = [self.pgr_name + ext for ext in self.file_ext]

        config_files = []
        # In current directory
        config_files += [name for name in config_file_names]
        config_files += ['.' + name for name in config_file_names]
        # In xdg_config_home
        config_files += [str(Path(xdg_config_home, self.pgr_name, 'config'))]
        config_files += [
            str(Path(xdg_config_home, self.pgr_name, name))
            for name in config_file_names
        ]
        # In home
        config_files += [
            str(Path(Path.home(), '.' + name)) for name in config_file_names
        ]
        # In xdg_config_dirs
        for config_dir in xdg_config_dirs:
            config_files += [
                str(Path(config_dir, name)) for name in config_file_names
            ]
        # In /etc
        config_files += [str(Path('/etc', self.pgr_name, 'config'))]
        config_files += [
            str(Path('/etc', self.pgr_name, name))
            for name in config_file_names
        ]
        config_files += [
            str(Path('/etc', name)) for name in config_file_names
        ]
        return config_files
