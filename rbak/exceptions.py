# Copyright 2017-2019 Rémy Taymans <remy.mieg@gmail.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Exceptions for rbak"""


class ServerError(Exception):
    """
    This exception means that something goes wrong with the save server
    """


class ConnectedUserError(ServerError):
    """
    This exception means that there is other users that are connected to
    the server and therefore the action required cannot be performed.
    """


class LockedError(ServerError):
    """
    This exception means that there is another process that has locked
    the server and therefore the action required cannot be performed.
    """


class NetworkError(ServerError):
    """This exception means that the network is unreachable."""
