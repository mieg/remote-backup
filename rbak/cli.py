# Copyright 2017-2019 Rémy Taymans <remy.mieg@gmail.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""CLI interface"""

from io import StringIO
import logging
import sys

import click
from click_spinner import spinner
import invoke

import rbak
from . import exceptions
from .core import backup as run_backup
from .core import SaveServer


LOG_FILENAME = "backup.log"

# Context settings
CONTEXT_SETTINGS = {'auto_envvar_prefix': rbak.PGRNAME.upper()}


# CLI commands

@click.group(context_settings=CONTEXT_SETTINGS)
@click.option(
    'conf',
    '--config',
    '-c',
    type=click.Path(exists=True),
    metavar=rbak.PGRNAME.upper() + 'CONF',
    help="%s config file." % rbak.PGRNAME
)
@click.option(
    'logfile',
    '--log',
    type=click.Path(),
    metavar='LOGFILE',
    help="Log file for rbak. See also --baklog"
)
@click.version_option(version=rbak.__version__)
def main(conf, logfile):
    """
    Manage backup on a remote backup server.

    To get help on a particular command run:

        rbak COMMAND --help
    """
    cfg = rbak.CONFIG
    if conf:
        cfg.config_files = [conf]
    cfg.load()

    if logfile:
        cfg.set('log', 'logfilename', logfile)

    # Logging
    logging.basicConfig(
        filename=cfg.get('log', 'filename', fallback=LOG_FILENAME),
        format='%(asctime)s - %(levelname)s : %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=20  # Loglevel INFO
    )


@main.command(short_help="Backup data.")
@click.option(
    'dryrun',
    '--dry-run',
    is_flag=True,
    help="Launch program but do not make changes."
)
@click.option(
    'baklogfile',
    '--baklog',
    type=click.Path(),
    metavar='LOGFILE',
    help="Log file for the data backuped."
)
@click.option(
    'shutdown',
    '--shutdown/--no-shutdown',
    is_flag=True,
    default=True,
    help="Do or do not shutdown the server at the end of the process."
)
@click.option(
    'sudo',
    '--sudo',
    is_flag=True,
    default=False,
    help="Use sudo to run the backup. Useful if data that does not "
    "belongs to the current user are being backuped."
)
def backup(dryrun, baklogfile, shutdown, sudo):
    """
    Backup your data to a remote server that will be waken up before the
    backup and shutdown when backup is performed.
    """
    invc = invoke.Context()

    password = ''
    if sudo:
        password = click.prompt("Sudo password", hide_input=True)
        # Test the given password, exit if the password is wrong
        try:
            invc.sudo('test 1', password=password, hide=True)
        except invoke.exceptions.Failure:
            click.echo("Error: Wrong sudo password.", err=True)
            sys.exit(1)

    # Load config file
    cfg = rbak.CONFIG

    if baklogfile:
        cfg.set('log', 'rsynclog', baklogfile)

    server = SaveServer(
        mac_address=cfg.get('server', 'mac'),
        broadcast=cfg.get('server', 'broadcast', fallback=None),
        address=cfg.get('server', 'address'),
        port=cfg.get('server', 'port'),
        username=cfg.get('server', 'username')
    )

    logging.info("Turn the server on")
    click.echo("Waiting server to turn on... ", nl=False)
    try:
        with spinner():
            server_is_on = server.turn_server_on()
    except exceptions.ServerError as err:
        logging.error(err)
        click.echo("Error: %s" % err, err=True)
        sys.exit(1)
    if not server_is_on:
        logging.error("Server is still unreachable.")
        click.echo("Fail")
        click.echo("Error: Server is still unreachable.", err=True)
        sys.exit(1)
    click.echo("Done")
    try:
        server.lock_server()
    except exceptions.ServerError as err:
        logging.error(err)
        click.echo("Error: %s" % err, err=True)
        sys.exit(1)
    logging.info("Run the backup")
    click.echo('Running the backup…')
    run_backup(cfg, server, sudo, password, dryrun)  # The save
    logging.info("Backup is done")
    click.echo('End of the backup')
    try:
        server.unlock_server()
    except exceptions.ServerError as err:
        logging.error(err)
        click.echo("Error: %s" % err, err=True)
        sys.exit(1)
    if shutdown:
        try:
            server.turn_server_off()
        except exceptions.LockedError as err:
            msg = (
                "Another machine is using the server. Leaving without "
                "shutdown."
            )
            logging.info(msg)
            click.echo(msg)
            logging.info("End")
            sys.exit(0)
        except exceptions.ConnectedUserError as err:
            msg = (
                "Another user is connected to the server. Leaving without "
                "shutdown."
            )
            logging.info(msg)
            click.echo(msg)
            logging.info("End")
            sys.exit(0)
        except exceptions.ServerError as err:
            logging.error(err)
            click.echo("Error: %s" % err, err=True)
            sys.exit(1)
        msg = "Turn off the server. The server will stop in one minute."
        logging.info(msg)
        click.echo(msg)
    logging.info("End")


@main.command()
@click.option(
    'force',
    '--force',
    is_flag=True,
    help="Force shutting server down, even if other process are running "
    "and other user are logged in."
)
def turn_off(force):
    """Turn off the save server."""
    cfg = rbak.CONFIG

    server = SaveServer(
        mac_address=cfg.get('server', 'mac'),
        broadcast=cfg.get('server', 'broadcast', fallback=None),
        address=cfg.get('server', 'address'),
        port=cfg.get('server', 'port'),
        username=cfg.get('server', 'username')
    )

    try:
        server.turn_server_off(force=force)
    except exceptions.LockedError as err:
        msg = (
            "Another machine is using the server. Leaving without "
            "shutdown."
        )
        logging.info(msg)
        click.echo(msg)
        logging.info("End")
        sys.exit(0)
    except exceptions.ConnectedUserError as err:
        msg = (
            "Another user is connected to the server. Leaving without "
            "shutdown."
        )
        logging.info(msg)
        click.echo(msg)
        logging.info("End")
        sys.exit(0)
    except exceptions.ServerError as err:
        logging.error(err)
        click.echo("Error: %s" % err, err=True)
        sys.exit(1)
    msg = "Turn off the server. The server will stop in one minute."
    logging.info(msg)
    click.echo(msg)


@main.command('config')
@click.option(
    '--source-files',
    is_flag=True,
    default=False,
    help="Show only configuration files that are loaded from "
    "the most important to the less important. Fail if there "
    "is no configuration file loaded."
)
@click.option(
    '--pager',
    is_flag=True,
    default=False,
    help="Send output to a pager."
)
@click.pass_context
def configuration(ctx, source_files, pager):
    """
    Show the current configuration.
    """
    cfg = rbak.CONFIG
    buffer = StringIO()

    cfg.write(buffer)

    if source_files:
        file_list = cfg.config_sources
        if not file_list:
            ctx.exit(1)  # No config file loaded
        # To get file from the most important to the less, we need to
        # reverse the list
        file_list.reverse()
        file_list_str = '\n'.join(file_list)
        if pager:
            click.echo_via_pager(file_list_str)
        else:
            click.echo(file_list_str)
    else:
        if pager:
            click.echo_via_pager(buffer.getvalue())
        else:
            click.echo(buffer.getvalue())
