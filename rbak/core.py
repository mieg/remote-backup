# Copyright 2017-2019 Rémy Taymans <remy.mieg@gmail.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Core tools for rbak"""

import socket
import sys
import time
import logging
from itertools import zip_longest

import click
import fabric
import invoke
from wakeonlan import send_magic_packet

from . import exceptions


class SaveServer():
    """The save server"""

    BEGIN_TIME = str(int(time.time()))

    def __init__(self, mac_address, address, port, username, broadcast=None):
        self.mac_address = mac_address
        self.broadcast = broadcast
        self.address = address
        self.port = int(port)
        self.username = username
        self.connected = False
        self.is_on = False
        self.error = None

    @property
    def connection(self):
        """Return an ssh connection to the server."""
        return fabric.Connection(
            host=self.address,
            user=self.username,
            port=self.port
        )

    def is_server_on(self, timeout=10):
        """Tell if the server is up or not."""
        try:
            s = socket.create_connection((self.address, self.port), timeout)
        except socket.error:
            return False
        s.close()
        return True

    def is_other_user_connected(self):
        """Check if other user is connected to the server"""
        result = None
        try:
            with self.connection as con:
                result = con.run('who', hide=True)
        except invoke.exceptions.Failure as err:
            raise exceptions.ServerError(
                "Cannot tell if there is other user connected.\n%s" % str(err)
            )
        return bool(result.stdout)

    def is_other_save_process_running(self):
        """
        Check if there is other client running a backup on the server.
        """
        result = None
        try:
            with self.connection as con:
                result = con.run('ls save-pid', hide=True)
        except invoke.exceptions.Failure as err:
            raise exceptions.ServerError(str(err))
        return bool(result.stdout)

    def lock_server(self):
        """Tell the server that this client is performing a backup"""
        filename = socket.gethostname() + '-' + self.BEGIN_TIME
        try:
            with self.connection as con:
                con.run('mkdir -p save-pid', hide=True)
                con.run('touch save-pid/%s' % filename, hide=True)
        except invoke.exceptions.Failure as err:
            raise exceptions.ServerError(
                "Cannot create lock file on server.\n%s" % str(err)
            )

    def unlock_server(self):
        """Tell the server that this client has finished his backup"""
        filename = socket.gethostname() + '-' + self.BEGIN_TIME
        try:
            with self.connection as con:
                con.run('rm save-pid/%s' % filename, hide=True)
        except invoke.exceptions.Failure as err:
            raise exceptions.ServerError(
                "Cannot remove lock file on server.\n%s" % str(err)
            )

    def send_wakeup_cmd(self):
        """Send magic packet on the network to wake up the server."""
        try:
            if self.broadcast:
                send_magic_packet(self.mac_address, ip_address=self.broadcast)
            else:
                send_magic_packet(self.mac_address, ip_address=self.address)
        except OSError as err:
            raise exceptions.NetworkError(str(err))

    def turn_server_on(self, timeout=120):
        """
        Turn on the server.
        Return True if the server is up before timeout is reached.
        """
        self.send_wakeup_cmd()
        begin = time.time()
        end = time.time()
        server_is_on = False
        while not server_is_on and end - begin < timeout:
            server_is_on = self.is_server_on()
            end = time.time()
        self.is_on = server_is_on
        return self.is_on

    def turn_server_off(self, force=False):
        """Turn off the server"""
        if not force and self.is_other_save_process_running():
            raise exceptions.LockedError()
        if not force and self.is_other_user_connected():
            raise exceptions.ConnectedUserError()
        try:
            with self.connection as con:
                con.run('sudo shutdown', hide=True)
        except invoke.exceptions.Failure as err:
            raise exceptions.ServerError(
                "Cannot shutdown the server.\n%s" % str(err)
            )

    def run(self, cmd):
        """Run a command on the server."""
        try:
            with self.connection as con:
                con.run(cmd, hide=True)
        except invoke.exceptions.Failure as err:
            raise exceptions.ServerError(
                "Fail to execute a command on the server.\n%s"
                % str(err)
            )


def backup(cfg, server, sudo=False, password='', dryrun=False):
    """Do the backup"""
    invc = invoke.Context()

    users_and_options = list(
        zip_longest(
            cfg.getlist('users', 'users'),
            cfg.getlist('users', 'options'),
            fillvalue=''
        )
    )
    dir_and_options = list(
        zip_longest(
            cfg.getlist('backups', 'directories'),
            cfg.getlist('backups', 'options'),
            fillvalue=''
        )
    )

    hostname = cfg.get('server', 'address')
    port = cfg.get('server', 'port')
    server_user = cfg.get('server', 'username')
    remote_dir = cfg.get('server', 'remote_dir')
    if not remote_dir.endswith('/'):
        remote_dir += '/'
    logfile = cfg.get('log', 'rsynclog')
    rsync_option = [
        'rsync',
        '-e',
        '"',
        'ssh',
        '-p',
        port,
        '-T',
        '-o Compression=no',
        '-o StrictHostKeyChecking=no',
        '-o UserKnownHostsFile=/dev/null',
        '"',
        '-aFhv --stats --progress --del',
        '--log-file',
        logfile,
    ]
    if dryrun:
        rsync_option.append('--dry-run')
    for (user, option) in users_and_options:
        mkdir_cmd = 'mkdir -p %s/home/%s/' % (remote_dir[:-1], user)
        logging.info(mkdir_cmd)
        try:
            server.run(mkdir_cmd)
        except exceptions.ServerError as err:
            logging.info(err)
            click.echo("Error: %s" % err, err=True)
            sys.exit(1)
        rsync_cmd = rsync_option[:]  # Copy the list using slicing
        rsync_cmd.append(option)
        rsync_cmd.append('/home/%s/' % user)
        rsync_cmd.append('%s@%s:%s/home/%s/' % (server_user,
                                                hostname,
                                                remote_dir[:-1],
                                                user))
        rsync_cmd = ' '.join(rsync_cmd)
        logging.info(rsync_cmd)
        click.echo(rsync_cmd)
        if sudo and password:
            invc.sudo(rsync_cmd, pty=True, password=password)
        else:
            invc.run(rsync_cmd, pty=True)
    for (folder, option) in dir_and_options:
        mkdir_cmd = 'mkdir -p %s%s/' % (remote_dir[:-1], folder)
        logging.info(mkdir_cmd)
        try:
            server.run(mkdir_cmd)
        except exceptions.ServerError as err:
            logging.info(err)
            click.echo("Error: %s" % err, err=True)
            sys.exit(1)
        rsync_cmd = rsync_option[:]  # Copy the list using slicing
        rsync_cmd.append(option)
        rsync_cmd.append(folder)
        rsync_cmd.append('%s@%s:%s%s' % (
            server_user,
            hostname,
            remote_dir[:-1],  # remote_dir without ending trailing slash
            folder
        ))
        rsync_cmd = ' '.join(rsync_cmd)
        logging.info(rsync_cmd)
        click.echo(rsync_cmd)
        if sudo and password:
            invc.sudo(rsync_cmd, pty=True, password=password)
        else:
            invc.run(rsync_cmd, pty=True)
