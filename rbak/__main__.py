"""Executed when module is run as a script."""

from . import cli


cli.main()  # pylint: disable=no-value-for-parameter
