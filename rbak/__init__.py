# Copyright 2017-2019 Rémy Taymans <remy.mieg@gmail.com>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

"""Constant shared across the module"""

from pathlib import Path

from . import settings


__productname__ = 'rbak'
__version__ = '0.6.0'
__copyright__ = "Copyright 2017-2019 Rémy Taymans"
__author__ = "Rémy Taymans"
__author_email__ = "remy.mieg@gmail.com"
__description__ = "Wake up a remote backup server and perform backup"
__url__ = "https://framagit.org/mieg/remote-backup"
__license__ = "AGPL-3.0+"

# Settings

PGRNAME = 'rbak'
DEFAULTSPATH = str(Path(__file__).parent / Path('defaults'))
DEFAULT_CONF = str(Path(DEFAULTSPATH, PGRNAME+'.conf'))

SETTINGS_PARAM = {
    'pgr_name': PGRNAME,
    'file_ext': ['.conf'],
    'defaults_file': DEFAULT_CONF,
    'default_section': 'general',
}

CONFIG = settings.SettingsManager(**SETTINGS_PARAM)
